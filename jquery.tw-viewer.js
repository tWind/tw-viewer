(function($) { 
	$.fn.twViewer = function(settings) {
		var options = $.extend({
			controlsClass: 'tw-controls', // кнопки контролей
			viewportClass: 'tw-viewport',
			wrapperClass: 'tw-wrapper',
			mode: 'vertical', // 'horizontal, vertical
			show: 5 // кол-во отображаемых блоков

		}, settings);

		var pane = this;
		var panePos = 0;
		// отдельный объект под переменные параметров
		var carousel = {
			// DOM elements templates
			template: {
				controls: function() {
					if(options.mode == 'horizontal') {
						return '<div class="' 
							+ options.controlsClass 
							+ '"><div class="left"></div><div class="right"></div></div>';
					}
					else {
						return '<div class="' 
							+ options.controlsClass 
							+ '"><div class="top"></div><div class="bottom"></div></div>';
					}
				},
				wrapper: '<div class="' + options.wrapperClass + '"></div>"',
				viewport: '<div class="' + options.viewportClass + '"></div>"'
			},
			build: function(buildMode) {

				//обертка целевого блока и добавление контролей
				$(pane).addClass('tw-items')
						.wrap(this.template.wrapper)
						.after(this.template.controls)
						.wrap(this.template.viewport);

				this.wrapper = $('.' + options.wrapperClass);
				this.viewport = $('.' + options.viewportClass);
				this.controls = $('.' + options.controlsClass).children();
				this.items = $(pane).children(); // блоки карусели

				$(this.wrapper).addClass(buildMode);

			}			 
		}

		var methods = {
			init: {
				horizontal: function() {					
					carousel.build('horizontal');
					
					carousel.step = $(carousel.items).outerWidth(); // шаг карусели == ширине блока
					carousel.paneWidth = options.show * carousel.step; // вычисление ширины viewport
					carousel.paneHeight = $(pane).outerHeight(); // вычисление высоты viewport
					// максимальная координата для остановки обработки клика по кнопке контроля
					carousel.maxPosX = carousel.paneWidth - carousel.items.length * carousel.step;
				
					// установка высоты viewport и ширины wrapper
					$(carousel.viewport).outerHeight(carousel.paneHeight);
					$(carousel.wrapper).outerWidth(carousel.paneWidth);
				},
				vertical: function() {
					carousel.build('vertical');
					
					carousel.step = $(carousel.items).outerHeight(); // шаг карусели == высоте блока
					carousel.paneHeight = options.show * carousel.step; // вычисление высоты viewport
					carousel.paneWidth = $(carousel.items).outerWidth(); // вычисление высоты viewport
					// максимальная координата для остановки обработки клика по кнопке контроля
					carousel.maxPosX = carousel.paneHeight - carousel.items.length * carousel.step;
				
					// установка высоты viewport и ширины wrapper
					$(carousel.viewport).outerHeight(carousel.paneHeight);
					$(carousel.wrapper).outerWidth(carousel.paneWidth);
				}
			},
			move: {
				left: function(step, pos) {
					if(pos < 0) {
						$(pane).stop().animate({ left: pos + step + "px" });
						pos += step;
					}
					return pos;
				},
				right: function(step, pos) {
					if(pos > carousel.maxPosX) {
						$(pane).stop().animate({ left: pos - step + "px" });
						pos -= step;
					}
					return pos;
				},
				top: function(step, pos) {
					if(pos < 0) {
						$(pane).stop().animate({ top: pos + step + "px" });
						pos += step;
					}
					return pos;
				},
				bottom: function(step, pos) {
					if(pos > carousel.maxPosX) {
						$(pane).stop().animate({ top: pos - step + "px" });
						pos -= step;
					}
					return pos;
				}
			}
			
		}
		
		//(!options.vertical) ? methods.init.horizontal() : methods.init.vertical();
		methods.init[options.mode]();
		
		// обработка клика
		// по классу определяется направление клика
		// move возвращает текущую позицию
		$(carousel.controls).on('click', function() {
			panePos = methods.move[$(this).attr('class')](carousel.step, panePos);
		});

		
	}

})(jQuery);





